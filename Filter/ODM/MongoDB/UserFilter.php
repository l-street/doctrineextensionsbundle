<?php

declare(strict_types=1);

namespace LStreet\DoctrineExtensionsBundle\Filter\ODM\MongoDB;

use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Query\Filter\BsonFilter;

class UserFilter extends BsonFilter
{

    /**
     * Gets the criteria array to add to a query.
     *
     * If there is no criteria for the class, an empty array should be returned.
     *
     * @param ClassMetadata $class
     * @return array
     */
    public function addFilterCriteria(ClassMetadata $class)
    {
        // TODO: Implement addFilterCriteria() method.
    }
}
