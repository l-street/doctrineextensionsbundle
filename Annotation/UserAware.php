<?php

declare(strict_types=1);

namespace LStreet\DoctrineExtensionsBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
final class UserAware
{
    /**
     * @var string
     */
    public $field = 'createdBy';
}
