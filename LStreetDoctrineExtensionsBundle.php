<?php

declare(strict_types=1);

namespace LStreet\DoctrineExtensionsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LStreetDoctrineExtensionsBundle extends Bundle
{
}
